#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "book_management.h"
#define CreateNode(p) p=(Book*)malloc(sizeof(Book));
#define DeleteNode(p) free((void*)p);

void RemoveNewLine(char* string){
	size_t length = strlen(string);
	if((length > 0)&&(string[length-1]=='\n')){
		string[length -1] = '\0';//TODO
	}
	return;
}

int allbook(Book*head, FILE*fp){
	Book*p, *last;
	//CreateNode(head);
	char savetitle[30];
	char bookauthor[30];
	char buff1[30];
	char buff2[30];
	char buff3[30];
	int countbooks = 0;
	last=head;
	//FILE * fp1= fopen("allbook.txt","r");
	while(!feof(fp)){
		fgets(buff1,30,fp);
		RemoveNewLine(buff1);
		fgets(savetitle,30,fp);
		RemoveNewLine(savetitle);
		fgets(bookauthor,30,fp);
		RemoveNewLine(bookauthor);
		fgets(buff2,30,fp);
		RemoveNewLine(buff2);
		fgets(buff3,30,fp);
		RemoveNewLine(buff3);
		CreateNode(p);
		p->title = (char*)malloc(sizeof(char));
		p->authors = (char*)malloc(sizeof(char));
		p->id=atoi(buff1);
		strcpy(p->title, savetitle);
		strcpy(p->authors, bookauthor);
		p->year=atoi(buff2);
		p->copies=atoi(buff3);
		countbooks++;
		//printf("Id is %d \n",p->id);
		//printf("title is %s \n",p->title);
		//printf("author is %s \n",p->authors);
		//printf("year is %d \n",p->year);
		//printf("copy is %d \n",p->copies);
		last->next=p;
		last=p;
	}
	last->next=NULL;
	return countbooks;

}



int registor();
int login();

void clean_flush(){
	int a;
	while((a = getchar()) != '\n' && a != EOF);
}
int find_by_year(Book*head){
	char search[30];
	int p = 0;
	printf("Please enter year: ");
	fgets(search,30,stdin);
	RemoveNewLine(search);
	setbuf(stdin,NULL);
	unsigned int y = atoi(search);
	Book*u = head->next;
	while(u){
		if((u->year == y) && (u->next != NULL)){
			printf("Id is %d \n",u->id);
			printf("title is %s \n",u->title);
			printf("author is %s \n",u->authors);
			printf("year is %d \n",u->year);
			printf("copy is %d \n",u->copies);
			p = 1;
			u = u->next;
		}else if(u->next == NULL){
			if(u->year == y){
				printf("Id is %d \n",u->id);
				printf("title is %s \n",u->title);
				printf("author is %s \n",u->authors);
				printf("year is %d \n",u->year);
				printf("copy is %d \n",u->copies);
				break;
			}else{
				if(p == 1){
					break;//TODO
				}
				printf("There are no books written in %d\n",y);
				return 0;
				break;
			}
		}else{
			u = u->next;
		}
	}
	return 1;
}

int find_by_title(Book* head){
	char search[30];
	int p = 0;
	printf("please enter the title: ");
	fgets(search, 30, stdin);
	RemoveNewLine(search);
	setbuf(stdin,NULL);
	Book*u = head->next;
	while(u){
		if(strcmp(search, u->title)==0 && u->next != NULL){
			printf("Id is %d \n",u->id);
			printf("title is %s \n",u->title);
			printf("author is %s \n",u->authors);
			printf("year is %d \n",u->year);
			printf("copy is %d \n",u->copies);
			p = 1;
			u = u->next;
		}else if(u-> next == NULL){
			if(strcmp(search, u->title)==0){
				printf("Id is %d \n",u->id);
				printf("title is %s \n",u->title);
				printf("author is %s \n",u->authors);
				printf("year is %d \n",u->year);
				printf("copy is %d \n",u->copies);
				break;//TODO
			}else{
				if(p == 1){
					break;//TODO
				}
				printf("There are no books entitled %s\n",search);
				return 0;
				break;
			}
		}else{
			u = u->next;
		}
	}
	return 1;
}


int find_by_author(Book* head){
	char search[30];
	int p = 0;
	printf("please enter the author: ");
	fgets(search, 30, stdin);
	RemoveNewLine(search);
	setbuf(stdin,NULL);
	Book*u = head->next;
	while(u){
		if(strcmp(search, u->authors)==0 && u->next != NULL){
			printf("Id is %d \n",u->id);
			printf("title is %s \n",u->title);
			printf("author is %s \n",u->authors);
			printf("year is %d \n",u->year);
			printf("copy is %d \n",u->copies);
			p = 1;
			u = u->next;
		}else if(u-> next == NULL){
			if(strcmp(search, u->authors)==0){
				printf("Id is %d \n",u->id);
				printf("title is %s \n",u->title);
				printf("author is %s \n",u->authors);
				printf("year is %d \n",u->year);
				printf("copy is %d \n",u->copies);
				break;//TODO
			}else{
				if(p == 1){
					break;//TODO
				}
				printf("There are no books entitled %s\n",search);
				return 0;
				break;
			}
		}else{
			u = u->next;
		}
	}
	return 1;
}


int mainpage(Book*head){
	int choice;
	printf("Please choose an option:\n");
	printf("1) Register an account\n");
	printf("2) Login\n");
	printf("3) Search for books\n");
	printf("4) Display all books\n");
	printf("5) Quit\n");
	printf(" Option:");
	scanf("%d", &choice);
	clean_flush();

	if(choice == 5){
		//clean_flush(stdin);
		return 0;
	}
	else if(choice == 1){
		//clean_flush(stdin);
		registor(head);
		return 1;
	}
	else if(choice == 2){
		login();
	}
	else if(choice == 3){
		printf("\n");
		printf("Loading.....");
		printf("\n");
		int choice2;
		printf("Please select a search method:\n");
		printf("1) Search by title\n");
		printf("2) Search by author\n");
		printf("3) Search by year\n");
		printf(" option:");
		scanf("%d", &choice2);
		setbuf(stdin,NULL);
		if(choice2 == 1){
			printf("\n");
			printf("Loading......");
			printf("\n");
			find_by_title(head);
		}else if(choice2 == 2){
			printf("\n");
			printf("Loading......");
			printf("\n");
			find_by_author(head);
		}else if(choice2 == 3){
			printf("\n");
			printf("Loading......");
			printf("\n");
			find_by_year(head);
		}else{
			printf("\n");
			printf("Loading......");
			printf("\n");
			printf("\n Please enter the correct number.\n");
		}
		
		printf("\n");
		printf("\n");
		mainpage(head);//TODO
	}
	else if(choice == 4){
		printf("\n");
		printf("Loading......");
		printf("\n");
		
		Book *t= head->next;
		while(t){
			printf("Id is %d \n",t->id);
			printf("title is %s \n",t->title);
			printf("author is %s \n",t->authors);
			printf("year is %d \n",t->year);
			printf("copy is %d \n",t->copies);
			t=t->next;
		}
		mainpage(head);//TODO
	}
	else{
		printf("\nSorry, the option you entered was invalid, please try again.\n\n");
		mainpage(head);
	}
}

int registor(Book*head){
	char username[30];
	char passward[30];
	FILE *fp = fopen("username_passward.txt","a+");;
	printf("Please enter a username: ");
	fgets(username, 30, stdin);
	setbuf(stdin,NULL);
	printf("Please enter a password: ");
	fgets(passward, 30, stdin);
	setbuf(stdin,NULL);

	while(!feof(fp)){
		char bidui[30];
		char k[30];
		fgets(bidui, 30, fp);
		RemoveNewLine(bidui);
		fgets(k, 30, fp);
		RemoveNewLine(k);
		if(strcmp(bidui, username) == 0){
			printf("Sorry, registration unsuccessful, the username you entered already exists.\n");
			mainpage(head);
		}

	}
	fputs(username, fp);
	fputs(passward, fp);
	printf("Registered library account successfully!\n");
	return 1;

}

int login(){
	FILE *fp;
	char username[30];
	char passward[30];
	fp = fopen("username_passward.txt","r");
	printf("Please enter a username: ");
	fgets(username, 30, stdin);
	RemoveNewLine(username);
	setbuf(stdin,NULL);
	printf("Please enter a password: ");
	fgets(passward, 30, stdin);
	RemoveNewLine(passward);
	setbuf(stdin,NULL);
	while(!feof(fp)){
		char yonghuming[30];
		char mima[30];
		fgets(yonghuming, 30, fp);
		fgets(mima, 30, fp);
		RemoveNewLine(yonghuming);
		RemoveNewLine(mima);
		if(strcmp(yonghuming, username) == 0 && strcmp(mima, passward) == 0){
			printf("\n(Logged in as: %s)\n", username);
			return 1;
		}
	}
	printf("Wrong passward, please try again.\n");

}



int main(){
	Book*head;
	CreateNode(head);
	FILE * fp1= fopen("allbook.txt","r");
	allbook(head,fp1);
	mainpage(head);
	
}
