#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "book_management.h"
#define CreateNode(p) p=(Book*)malloc(sizeof(Book));
#define DeleteNode(p) free((void*)p);




int add(Book book,Book *head,Loan*h,Login*k){
	char save_for_title[30];
	char save_for_author[30];
	char save_for_year[30];
	char save_for_copies[30];


	printf("Enter the title of book you wish to add: ");
	fgets(save_for_title, 30, stdin);
	RemoveNewLine(save_for_title);
	setbuf(stdin,NULL);
	printf("Enter the author of the book you wish to add: ");
	fgets(save_for_author,30,stdin);
	setbuf(stdin,NULL);
	RemoveNewLine(save_for_author);
	printf("Enter the year that the book you wish to add was released: ");
	fgets(save_for_year,30,stdin);
	setbuf(stdin,NULL);
	RemoveNewLine(save_for_year);
	printf("Enter the number of copies of the book you wish to add: ");
	fgets(save_for_copies, 30, stdin);
	setbuf(stdin,NULL);
	RemoveNewLine(save_for_copies);

	book.title = save_for_title;
	book.authors = save_for_author;
	book.year = atoi(save_for_year);
	book.copies = atoi(save_for_copies);

	Book *n= head->next;
	while(n){

		if(n->title == book.title){
			printf("\nThe book has already existed.\n");
			return 0;
		}
		n = n->next;
	}

	Book*tail = head;
	while(tail->next != NULL){
		tail = tail->next;
	}

	Book*insert;
	CreateNode(insert);
	tail->next=insert;

	insert->title = (char*)malloc(sizeof(char));
	insert->authors = (char*)malloc(sizeof(char));
	insert->id = tail->id + 1;
	strcpy(insert->title, book.title);
	strcpy(insert->authors, save_for_author);
	insert->year = atoi(save_for_year);
	insert->copies = atoi(save_for_copies);
	insert->next = NULL;

	printf("\n Add successfully.\n");
	/*test
	Book *t= head->next;
	while(t){
	printf("Id is %d \n",t->id);
	printf("title is %s \n",t->title);
	printf("author is %s \n",t->authors);
	printf("year is %d \n",t->year);
	printf("copy is %d \n",t->copies);
	t=t->next;
	}
	 */
	printf("\n");
	printf("\n");
	librarian_view(head,h,k);
}

int remove_a_book(Book book,Book* head,Loan*m,Login*kk){
	Book*t = head;
	Book*h = head;
	Book*q;
	int o = 0;
	t = t->next;
	while(t != NULL){
		if (t-> copies == 0){
			printf("Some books have been checked out\n");
			librarian_view(head,m,kk);
		}
		if(t->id == book.id){
			h->next = t->next;
			free(t);
			o = 1;
			q = h->next;
			break;
		}else{
			t = t->next;
			h = h->next;
		}

	}
	if(o == 0){
		printf("\nThere is not such a book\n");
		return 0;
	}
	while(q != NULL){
		q->id = q->id - 1;
		q = q->next;
	}

	printf("\nRemove successfully\n");
	/*test
	Book *c= head->next;
	while(c){
	printf("Id is %d \n",c->id);
	printf("title is %s \n",c->title);
	printf("author is %s \n",c->authors);
	printf("year is %d \n",c->year);
	printf("copy is %d \n",c->copies);
	c=c->next;
	}
	 */
	printf("\n");
	printf("\n");
	librarian_view(head,m,kk);
}

void librarian_view(Book*head,Loan*h,Login*k){
	int choice;
	printf("\nPlease choose an option:\n");
	printf("1) Add a book\n");
	printf("2) Remove a book\n");
	printf("3) Search for books\n");
	printf("4) Display all books\n");
	printf("5) Log out\n");
	printf("  Option: ");
	scanf("%d", &choice);
	setbuf(stdin,NULL);
	if(choice == 1){
		Book book;
		add(book,head,h,k);
	}
	else if(choice == 2){
		int f;
		Book book2;
		printf("\nPlease enter the id of the book: ");
		scanf("%d", &f);
		setbuf(stdin,NULL);
		book2.id = f;
		remove_a_book(book2,head,h,k);
	}
	else if(choice ==3){

		printf("\n");
		printf("Loading.....");
		printf("\n");
		int choice2;
		printf("\nPlease select a search method:\n");
		printf("1) Search by title\n");
		printf("2) Search by author\n");
		printf("3) Search by year\n");
		printf(" option:");
		scanf("%d", &choice2);
		setbuf(stdin,NULL);
		if(choice2 == 1){
			printf("\n");
			printf("Loading......");
			printf("\n");
			find_by_title(head);
		}else if(choice2 == 2){
			printf("\n");
			printf("Loading......");
			printf("\n");
			find_by_author(head);
		}else if(choice2 == 3){
			printf("\n");
			printf("Loading......");
			printf("\n");
			find_by_year(head);
		}else{
			printf("\n");
			printf("Loading......");
			printf("\n");
			printf("\n Please enter the correct number.\n");
		}

		printf("\n");
		printf("\n");
		librarian_view(head,h,k);
	}
	else if(choice ==4){

		printf("\n");
		printf("Loading......");
		printf("\n");
		printf("\n%-5s%-30s%-30s%-10s%-10s\n","ID","Title","Authors","year","copies");
		Book *t= head->next;
		while(t){
			printf("%-5d%-30s%-30s%-10d%-10d\n",t->id,t->title,t->authors,t->year,t->copies);
			t=t->next;
		}
		printf("\n");
		librarian_view(head,h,k);
	}
	else if(choice == 5){
		mainpage(head,h,k);
	}
	else{
		printf("\n Please enter the correct number.\n");
	}
}

int librarian(Book*head,Loan*h,Login*k){


	librarian_view(head,h,k);

}

