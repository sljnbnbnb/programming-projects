#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "book_management.h"
#define CreateNode(p) p=(Book*)malloc(sizeof(Book));
#define DeleteNode(p) free((void*)p);


typedef struct node{int data; struct node*next;}NodeTp;

void RemoveNewLine(char* string){
	size_t length = strlen(string);
	if((length > 0)&&(string[length-1]=='\n')){
		string[length -1] = '\0';//TODO
	}
	return;
}

void clean_flush(){
	int a;
	while((a = getchar()) != '\n' && a != EOF);
}

int allbook(Book*head, FILE*fp){
	Book*p, *last;
	char savetitle[30];
	char bookauthor[30];
	char buff1[30];
	char buff2[30];
	char buff3[30];
	int countbooks = 0;
	last=head;
	while(fgets(buff1,30,fp)){

		RemoveNewLine(buff1);
		fgets(savetitle,30,fp);
		RemoveNewLine(savetitle);
		fgets(bookauthor,30,fp);
		RemoveNewLine(bookauthor);
		fgets(buff2,30,fp);
		RemoveNewLine(buff2);
		fgets(buff3,30,fp);
		RemoveNewLine(buff3);
		CreateNode(p);
		p->title = (char*)malloc(sizeof(char));
		p->authors = (char*)malloc(sizeof(char));
		p->id=atoi(buff1);
		strcpy(p->title, savetitle);
		strcpy(p->authors, bookauthor);
		p->year=atoi(buff2);
		p->copies=atoi(buff3);
		countbooks++;
		last->next=p;
		last=p;
	}
	last->next=NULL;
	return countbooks;

}
int bookloan(Loan*head, FILE*fp){
	Loan*p, *last;
	char user[30];
	char savetitle[30];

	last=head;
	while(fgets(user,30,fp)){
		RemoveNewLine(user);
		fgets(savetitle,30,fp);
		RemoveNewLine(savetitle);


		p=(Loan*)malloc(sizeof(Loan));
		p->users = (char*)malloc(sizeof(char));
		p->title = (char*)malloc(sizeof(char));

		strcpy(p->users, user);
		strcpy(p->title, savetitle);


		last->next=p;
		last=p;
	}
	last->next=NULL;

}

int userpassward(Login*k, FILE*fp){
	Login*p, *last;
	char user[30];
	char passwards[30];

	last=k;
	while(fgets(user,30,fp)){
		RemoveNewLine(user);
		fgets(passwards,30,fp);
		RemoveNewLine(passwards);


		p=(Login*)malloc(sizeof(Login));
		p->users = (char*)malloc(sizeof(char));
		p->passward = (char*)malloc(sizeof(char));

		strcpy(p->users, user);
		strcpy(p->passward, passwards);


		last->next=p;
		last=p;
	}
	last->next=NULL;

}

int storebooks(Book*head, FILE*fp){
	Book*ct = head ->next;
	while(ct){
		fprintf(fp,"%d\n",ct->id);
		fprintf(fp,"%s\n",ct->title);
		fprintf(fp,"%s\n",ct->authors);
		fprintf(fp,"%d\n",ct->year);
		fprintf(fp,"%d\n",ct->copies);
		ct = ct ->next;
	}
}

int storeloan(Loan*h, FILE*fp){
	Loan*ct = h ->next;
	while(ct){
		fprintf(fp,"%s\n",ct->users);
		fprintf(fp,"%s\n",ct->title);
		ct = ct ->next;
	}
}

int storeusers(Login*k, FILE*fp){
	Login*ct = k ->next;
	while(ct){
		fprintf(fp,"%s\n",ct->users);
		fprintf(fp,"%s\n",ct->passward);
		ct = ct ->next;
	}
}

//end of the initialization


//all functions about finding books


int find_by_year(Book*head){
	char search[30];
	int p = 0;
	printf("Please enter year: ");
	fgets(search,30,stdin);
	RemoveNewLine(search);
	setbuf(stdin,NULL);
	unsigned int y = atoi(search);
	Book*u = head->next;
	while(u){
		if((u->year == y) && (u->next != NULL)){
			printf("\nId is %d \n",u->id);
			printf("title is %s \n",u->title);
			printf("author is %s \n",u->authors);
			printf("year is %d \n",u->year);
			printf("copy is %d \n",u->copies);
			p = 1;
			u = u->next;
		}else if(u->next == NULL){
			if(u->year == y){
				printf("\nId is %d \n",u->id);
				printf("title is %s \n",u->title);
				printf("author is %s \n",u->authors);
				printf("year is %d \n",u->year);
				printf("copy is %d \n",u->copies);
				break;
			}else{
				if(p == 1){
					break;//TODO
				}
				printf("There are no books written in %d\n",y);
				return 0;
				break;
			}
		}else{
			u = u->next;
		}
	}
	return 1;
}

int find_by_title(Book* head){
	char search[30];
	int p = 0;
	printf("please enter the title: ");
	fgets(search, 30, stdin);
	RemoveNewLine(search);
	setbuf(stdin,NULL);
	Book*u = head->next;
	while(u){
		if(strcmp(search, u->title)==0 && u->next != NULL){
			printf("\nId is %d \n",u->id);
			printf("title is %s \n",u->title);
			printf("author is %s \n",u->authors);
			printf("year is %d \n",u->year);
			printf("copy is %d \n",u->copies);
			p = 1;
			u = u->next;
		}else if(u-> next == NULL){
			if(strcmp(search, u->title)==0){
				printf("\nId is %d \n",u->id);
				printf("title is %s \n",u->title);
				printf("author is %s \n",u->authors);
				printf("year is %d \n",u->year);
				printf("copy is %d \n",u->copies);
				break;//TODO
			}else{
				if(p == 1){
					break;//TODO
				}
				printf("There are no books entitled %s\n",search);
				return 0;
				break;
			}
		}else{
			u = u->next;
		}
	}
	return 1;
}


int find_by_author(Book* head){
	char search[30];
	int p = 0;
	printf("please enter the author: ");
	fgets(search, 30, stdin);
	RemoveNewLine(search);
	setbuf(stdin,NULL);
	Book*u = head->next;
	while(u){
		if(strcmp(search, u->authors)==0 && u->next != NULL){
			printf("\nId is %d \n",u->id);
			printf("title is %s \n",u->title);
			printf("author is %s \n",u->authors);
			printf("year is %d \n",u->year);
			printf("copy is %d \n",u->copies);
			p = 1;
			u = u->next;
		}else if(u-> next == NULL){
			if(strcmp(search, u->authors)==0){
				printf("\nId is %d \n",u->id);
				printf("title is %s \n",u->title);
				printf("author is %s \n",u->authors);
				printf("year is %d \n",u->year);
				printf("copy is %d \n",u->copies);
				break;//TODO
			}else{
				if(p == 1){
					break;//TODO
				}
				printf("There are no books entitled %s\n",search);
				return 0;
				break;
			}
		}else{
			u = u->next;
		}
	}
	return 1;
}


//the main page and registration functions

int registor(Book*head,Loan*h,Login*k){
	char username[30];
	char passward[30];
	Login* g = k->next;
	printf("\nPlease enter a username: ");
	fgets(username, 30, stdin);
	setbuf(stdin,NULL);
	RemoveNewLine(username);
	printf("Please enter a password: ");
	fgets(passward, 30, stdin);
	setbuf(stdin,NULL);
	RemoveNewLine(passward);
	while(g){
		if(strcmp(username,g->users) == 0 && g->next != NULL){
			printf("The username is already existed\n");
			return 0;
		}else if(g->next == NULL){
			if(strcmp(username,g->users) == 0){
				printf("The username is already existed\n");
				return 0;
			}else{
				Login*insert;
				insert=(Login*)malloc(sizeof(Login));;
				g->next=insert;

				insert->passward = (char*)malloc(sizeof(char));
				insert->users = (char*)malloc(sizeof(char));
				strcpy(insert->users, username);
				strcpy(insert->passward, passward);
				insert->next = NULL;
				printf("\nRegistered library account successfully!\n");
				return 1;
			}
		}else{
			g = g->next;
		}
	}
}

int login(Book*head,Loan*h,Login*k){

	char username[30];
	char passward[30];
	char guanliyuan[30] = "librarian";
	printf("Please enter a username: ");
	fgets(username, 30, stdin);
	RemoveNewLine(username);
	setbuf(stdin,NULL);
	printf("Please enter a password: ");
	fgets(passward, 30, stdin);
	RemoveNewLine(passward);
	setbuf(stdin,NULL);
	RemoveNewLine(guanliyuan);
	Login*u = k->next;
	while(u){
		if(strcmp(username, u->users)==0 && strcmp(passward, u->passward)==0 && u->next != NULL){
			printf("\n(Logged in as: %s)", username);
			if(strcmp(username,guanliyuan)==0 && strcmp(passward,guanliyuan)==0){
				librarian(head,h,k);
				return 1;
			}else{
				user(head,h,k);
				return 2;
			}
		}else if(u-> next == NULL){
			if(strcmp(username, u->users)==0 && strcmp(passward, u->passward)==0){
				printf("\n(Logged in as: %s)", username);
				if(strcmp(username,guanliyuan)==0 && strcmp(passward,guanliyuan)==0){
					librarian(head,h,k);
					return 1;
				}else{
					user(head,h,k);
					return 2;
				}
				break;//TODO
			}else{
				printf("Please check your username and password.\n");
				mainpage(head,h,k);
				return 0;
				break;
			}
		}else{
			u = u->next;
		}
	}
	return 1;


}


int mainpage(Book*head,Loan*h,Login*k){
	int choice;
	printf("\nPlease choose an option:\n");
	printf("1) Register an account\n");
	printf("2) Login\n");
	printf("3) Search for books\n");
	printf("4) Display all books\n");
	printf("5) Quit\n");
	printf(" Option:");
	scanf("%d", &choice);
	clean_flush();

	if(choice == 5){
		
		return 0;
	}
	else if(choice == 1){
		registor(head,h,k);
		mainpage(head,h,k);
	}
	else if(choice == 2){
		login(head,h,k);
	}
	else if(choice == 3){
		printf("\n");
		printf("Loading.....");
		printf("\n");
		int choice2;
		printf("\nPlease select a search method:\n");
		printf("1) Search by title\n");
		printf("2) Search by author\n");
		printf("3) Search by year\n");
		printf(" option:");
		scanf("%d", &choice2);
		setbuf(stdin,NULL);
		if(choice2 == 1){
			printf("\n");
			printf("Loading......");
			printf("\n");
			find_by_title(head);
		}else if(choice2 == 2){
			printf("\n");
			printf("Loading......");
			printf("\n");
			find_by_author(head);
		}else if(choice2 == 3){
			printf("\n");
			printf("Loading......");
			printf("\n");
			find_by_year(head);
		}else{
			printf("\n");
			printf("Loading......");
			printf("\n");
			printf("\n Please enter the correct number.\n");
		}

		printf("\n");
		printf("\n");
		mainpage(head,h,k);//TODO
	}
	else if(choice == 4){
		printf("\n");
		printf("Loading......");
		printf("\n");
		printf("\n%-5s%-30s%-30s%-10s%-10s\n","ID","Title","Authors","year","copies");
		Book *t= head->next;
		while(t){
			printf("%-5d%-30s%-30s%-10d%-10d\n",t->id,t->title,t->authors,t->year,t->copies);
			t=t->next;
		}
		mainpage(head,h,k);//TODO
	}
	else{
		printf("\nSorry, the option you entered was invalid, please try again.\n");
		mainpage(head,h,k);
	}
}

//the main function


int main(int argc,char*argv[]){
	FILE*fp1;
	FILE*fp2;
	FILE*fp3;
	if(argc != 4){
		printf("file error!");
		return 0;
	}
	fp1= fopen(argv[1],"r");
	fp2= fopen(argv[2],"r");
	fp3= fopen(argv[3],"r");

	Book*head;
	CreateNode(head);
	allbook(head,fp1);


	Loan*h;
	h=(Loan*)malloc(sizeof(Loan));
	bookloan(h,fp2);

	Login*k;
	k=(Login*)malloc(sizeof(Login));
	userpassward(k,fp3);

	fclose(fp1);
	fclose(fp2);
	fclose(fp3);
	mainpage(head,h,k);
	FILE*fp5 = fopen(argv[1],"w");
	FILE*fp6 = fopen(argv[2],"w");
	FILE*fp7 = fopen(argv[3],"w");
	storebooks(head,fp5);
	storeloan(h,fp6);
	storeusers(k,fp7);
	fclose(fp5);
	fclose(fp6);
	fclose(fp7);

}
