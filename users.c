#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "book_management.h"
#define CreateNode(p) p=(Book*)malloc(sizeof(Book));
#define DeleteNode(p) free((void*)p);



int borrow_book(Book*head,Loan*h){
	char search[30];
	char titles[30];
	printf("\nplease enter the user name: ");
	fgets(search, 30, stdin);
	RemoveNewLine(search);
	setbuf(stdin,NULL);
	Loan*u = h->next;
	while(u){
		if(strcmp(search, u->users)==0 && u->next != NULL){
			printf("\nYou have already borrowed a book: %s\n",u->title);

			break;
		}else if(u-> next == NULL){
			if(strcmp(search, u->users)==0){
				printf("\nYou have already borrowed a book: %s\n",u->title);
				break;
			}else{
				printf("\nEnter the title of book you wish to borrow: ");
				fgets(titles, 30, stdin);
				RemoveNewLine(titles);
				setbuf(stdin,NULL);
				int o;
				Book*m = head->next;
				while(m){
					if((strcmp(m->title, titles)==0) && (m->next != NULL)){
						if(m->copies == 0){
							printf("\nSorry, this book has been lent out in full\n");
							o=0;
							break;
						}else{
							m->copies = m->copies - 1;
							o = 1;
							break;
						}

					}else if(m->next == NULL){
						if(strcmp(m->title, titles)==0){
							if(m->copies == 0){
								printf("\nSorry, this book has been lent out in full\n");
								o=0;
								break;
							}else{
								m->copies = m->copies - 1;
								o = 1;
								break;
							}

						}else{
							printf("\nThere is not such a book\n");
							o=0;
							break;
						}
					}else{
						m = m->next;
					}
				}
				if(o == 0){
					break;
				}
				if(o == 1){
					Loan*insert;
					insert=(Loan*)malloc(sizeof(Loan));;
					u->next=insert;

					insert->title = (char*)malloc(sizeof(char));
					insert->users = (char*)malloc(sizeof(char));
					strcpy(insert->title, titles);
					strcpy(insert->users, search);
					insert->next = NULL;

					printf("\n Borrow successfully.\n");


					break;
				}
			}
		}else{
			u = u->next;
		}
	}

}

int return_book(Book*head,Loan*h){
	char return_title[30];
	char search[30];
	printf("\nplease enter the user name: ");
	fgets(search, 30, stdin);
	RemoveNewLine(search);
	setbuf(stdin,NULL);
	Loan*u = h->next;
	Loan*t = h;
	while(u){
		if(strcmp(search, u->users)==0 && u->next != NULL){
			printf("\nplease enter the title: ");
			fgets(return_title, 30, stdin);
			RemoveNewLine(return_title);
			setbuf(stdin,NULL);
			if(strcmp(return_title, u->title)==0){
				Book* v = head->next;
				while(v){
					if(strcmp(return_title, v->title)==0 ){
						v->copies = v->copies + 1;
						printf("\nReturn successfully!\n");
						break;
					}else{
						v = v->next;
					}
				}
				t->next = u->next;
				DeleteNode(u);
				break;
			}

		}else if(u-> next == NULL){
			if(strcmp(search, u->users)==0){
				printf("\nplease enter the title: ");
				fgets(return_title, 30, stdin);
				RemoveNewLine(return_title);
				setbuf(stdin,NULL);
				if(strcmp(return_title, u->title)==0){
					Book* v = head->next;
					while(v){
						if(strcmp(return_title, v->title)==0 ){
							v->copies = v->copies + 1;
							printf("\nReturn successfully!\n");
							break;
						}else{
							v = v->next;
						}
					}
					t->next = u->next;
					DeleteNode(u);
					break;
				}
			}else{
				printf("\nThe user name is wrong\n");
				break;

			}
		}else{
			u = u->next;
			t = t->next;
		}
	}


}

void user_view(Book*head,Loan*h,Login*k){
	int choice;
	printf("\nPlease choose an option:\n");
	printf("1) Borrow a book\n");
	printf("2) Return a book\n");
	printf("3) Search for books\n");
	printf("4) Display all books\n");
	printf("5) Log out\n");
	printf("  Option: ");
	scanf("%d", &choice);
	setbuf(stdin,NULL);
	if(choice == 1){
		borrow_book(head,h);
		user_view(head,h,k);//mainview
	}
	else if(choice == 2){
		return_book(head,h);
		printf("\n");

		user_view(head,h,k);//mainview
	}
	else if(choice ==3){

		printf("\n");
		printf("Loading.....");
		printf("\n");
		int choice2;
		printf("\nPlease select a search method:\n");
		printf("1) Search by title\n");
		printf("2) Search by author\n");
		printf("3) Search by year\n");
		printf(" option:");
		scanf("%d", &choice2);
		setbuf(stdin,NULL);
		if(choice2 == 1){
			printf("\n");
			printf("Loading......");
			printf("\n");
			find_by_title(head);
		}else if(choice2 == 2){
			printf("\n");
			printf("Loading......");
			printf("\n");
			find_by_author(head);
		}else if(choice2 == 3){
			printf("\n");
			printf("Loading......");
			printf("\n");
			find_by_year(head);
		}else{
			printf("\n");
			printf("Loading......");
			printf("\n");
			printf("\n Please enter the correct number.\n");
		}

		printf("\n");
		printf("\n");
		user_view(head,h,k);
	}
	else if(choice ==4){

		printf("\n");
		printf("Loading......");
		printf("\n");
		printf("\n%-5s%-30s%-30s%-10s%-10s\n","ID","Title","Authors","year","copies");
		Book *t= head->next;
		while(t){
			printf("%-5d%-30s%-30s%-10d%-10d\n",t->id,t->title,t->authors,t->year,t->copies);
			t=t->next;
		}
		printf("\n");
		user_view(head,h,k);
	}
	else if(choice == 5){
		mainpage(head,h,k);
	}
	else{
		printf("\n Please enter the correct number.\n");
	}
}



int user(Book*head,Loan*h,Login*k){

	user_view(head,h,k);
}




