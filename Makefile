
# code details

EXE = ./library
SRC= interface.c users.c librarian.c

# generic build details

CC=      gcc
CFLAGS= -std=c99 -Wall
CLINK= 

# compile to object code

OBJ= $(SRC:.c=.o)

.c.o:
	$(CC) $(CFLAGS) -c -o $@ $<

# build executable: type 'make'

$(EXE): $(OBJ)
	$(CC) $(OBJ) $(CLINK) -o $(EXE) 

all:
	$(CC) $(CFLAGS) $(SRC) -o $(EXE)

# clean up and remove object code and executable: type 'make clean'

clean:
	rm -f $(OBJ) $(EXE)

# dependencies

interface.o:	interface.c book_management.h
users.o:  users.c book_management.h
librarian.o: librarian.c book_management.h
