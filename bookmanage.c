#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "book_management.h"
#define CreateNode(p) p=(Book*)malloc(sizeof(Book));
#define DeleteNode(p) free((void*)p);

typedef struct node{int data; struct node*next}NodeTp;

void librarian_view(Book*head);

void RemoveNewLine(char* string){
	size_t length = strlen(string);
	if((length > 0)&&(string[length-1]=='\n')){
		string[length -1] = '\0';//TODO
	}
	return;
}

void clean_flush(){
	int a;
	while((a = getchar()) != '\n' && a != EOF);
}

int allbook(Book*head, FILE*fp){
	Book*p, *last;
	//CreateNode(head);
	char savetitle[30];
	char bookauthor[30];
	char buff1[30];
	char buff2[30];
	char buff3[30];
	int countbooks = 0;
	last=head;
	//FILE * fp1= fopen("allbook.txt","r");
	while(!feof(fp)){
		fgets(buff1,30,fp);
		RemoveNewLine(buff1);
		fgets(savetitle,30,fp);
		RemoveNewLine(savetitle);
		fgets(bookauthor,30,fp);
		RemoveNewLine(bookauthor);
		fgets(buff2,30,fp);
		RemoveNewLine(buff2);
		fgets(buff3,30,fp);
		RemoveNewLine(buff3);
		CreateNode(p);
		p->title = (char*)malloc(sizeof(char));
		p->authors = (char*)malloc(sizeof(char));
		p->id=atoi(buff1);
		strcpy(p->title, savetitle);
		strcpy(p->authors, bookauthor);
		p->year=atoi(buff2);
		p->copies=atoi(buff3);
		countbooks++;
		//printf("Id is %d \n",p->id);
		//printf("title is %s \n",p->title);
		//printf("author is %s \n",p->authors);
		//printf("year is %d \n",p->year);
		//printf("copy is %d \n",p->copies);
		last->next=p;
		last=p;
	}
	last->next=NULL;
	return countbooks;

}


int add(Book book,Book *head){
	char save_for_title[30];
	char save_for_author[30];
	char save_for_year[30];
	char save_for_copies[30];


	printf("Enter the title of book you wish to add: ");
	fgets(save_for_title, 30, stdin);
	RemoveNewLine(save_for_title);
	setbuf(stdin,NULL);
	printf("Enter the author of the book you wish to add: ");
	fgets(save_for_author,30,stdin);
	setbuf(stdin,NULL);
	RemoveNewLine(save_for_author);
	printf("Enter the year that the book you wish to add was released: ");
	fgets(save_for_year,30,stdin);
	setbuf(stdin,NULL);
	RemoveNewLine(save_for_year);
	printf("Enter the number of copies of the book you wish to add: ");
	fgets(save_for_copies, 30, stdin);
	setbuf(stdin,NULL);
	RemoveNewLine(save_for_copies);

	book.title = save_for_title;
	book.authors = save_for_author;
	book.year = atoi(save_for_year);
	book.copies = atoi(save_for_copies);

	Book *n= head->next;
	while(n){

		if(n->title == book.title){
			printf("\nThe book has already existed.\n");
			return 0;
		}
		n = n->next;
	}

	Book*tail = head;
	while(tail->next != NULL){
		tail = tail->next;
	}

		Book*insert;
		CreateNode(insert);
		tail->next=insert;

		insert->title = (char*)malloc(sizeof(char));
		insert->authors = (char*)malloc(sizeof(char));
		//int l = tail->id++;
		//tail->next = insert;
		insert->id = tail->id + 1;
		strcpy(insert->title, book.title);
		strcpy(insert->authors, save_for_author);
		insert->year = atoi(save_for_year);
		insert->copies = atoi(save_for_copies);
		insert->next = NULL;

	printf("\n Add successfully.\n");

	Book *t= head->next;
	while(t){
		printf("Id is %d \n",t->id);
		printf("title is %s \n",t->title);
		printf("author is %s \n",t->authors);
		printf("year is %d \n",t->year);
		printf("copy is %d \n",t->copies);
		t=t->next;
	}

	printf("\n");
	printf("\n");
	librarian_view(head);
}

int remove_a_book(Book book,Book* head){
	Book*t = head;
	Book*h = head;
	Book*q;
	int o = 0;
	t = t->next;
	while(t != NULL){
		/*注意这里少一个判断能否remove的条件*/
		if(t->id == book.id){
			h->next = t->next;
			free(t);
			o = 1;
			q = h->next;
			break;
		}else{
		t = t->next;
		h = h->next;
		}

	}
	if(o == 0){
		printf("\nThere is not such a book\n");
		return 0;
	}
	while(q != NULL){
		q->id = q->id - 1;
		q = q->next;
	}

	printf("\nRemove successfully\n");

	Book *c= head->next;
	while(c){
		printf("Id is %d \n",c->id);
		printf("title is %s \n",c->title);
		printf("author is %s \n",c->authors);
		printf("year is %d \n",c->year);
		printf("copy is %d \n",c->copies);
		c=c->next;
	}

	printf("\n");
	printf("\n");
	librarian_view(head);
}


int find_by_year(Book*head){
	char search[30];
	int p = 0;
	printf("Please enter year: ");
	fgets(search,30,stdin);
	RemoveNewLine(search);
	setbuf(stdin,NULL);
	unsigned int y = atoi(search);
	Book*u = head->next;
	while(u){
		if((u->year == y) && (u->next != NULL)){
			printf("Id is %d \n",u->id);
			printf("title is %s \n",u->title);
			printf("author is %s \n",u->authors);
			printf("year is %d \n",u->year);
			printf("copy is %d \n",u->copies);
			p = 1;
			u = u->next;
		}else if(u->next == NULL){
			if(u->year == y){
				printf("Id is %d \n",u->id);
				printf("title is %s \n",u->title);
				printf("author is %s \n",u->authors);
				printf("year is %d \n",u->year);
				printf("copy is %d \n",u->copies);
				break;
			}else{
				if(p == 1){
					break;//TODO
				}
				printf("There are no books written in %d\n",y);
				return 0;
				break;
			}
		}else{
			u = u->next;
		}
	}
	return 1;
}

int find_by_title(Book* head){
	char search[30];
	int p = 0;
	printf("please enter the title: ");
	fgets(search, 30, stdin);
	RemoveNewLine(search);
	setbuf(stdin,NULL);
	Book*u = head->next;
	while(u){
		if(strcmp(search, u->title)==0 && u->next != NULL){
			printf("Id is %d \n",u->id);
			printf("title is %s \n",u->title);
			printf("author is %s \n",u->authors);
			printf("year is %d \n",u->year);
			printf("copy is %d \n",u->copies);
			p = 1;
			u = u->next;
		}else if(u-> next == NULL){
			if(strcmp(search, u->title)==0){
				printf("Id is %d \n",u->id);
				printf("title is %s \n",u->title);
				printf("author is %s \n",u->authors);
				printf("year is %d \n",u->year);
				printf("copy is %d \n",u->copies);
				break;//TODO
			}else{
				if(p == 1){
					break;//TODO
				}
				printf("There are no books entitled %s\n",search);
				return 0;
				break;
			}
		}else{
			u = u->next;
		}
	}
	return 1;
}


int find_by_author(Book* head){
	char search[30];
	int p = 0;
	printf("please enter the author: ");
	fgets(search, 30, stdin);
	RemoveNewLine(search);
	setbuf(stdin,NULL);
	Book*u = head->next;
	while(u){
		if(strcmp(search, u->authors)==0 && u->next != NULL){
			printf("Id is %d \n",u->id);
			printf("title is %s \n",u->title);
			printf("author is %s \n",u->authors);
			printf("year is %d \n",u->year);
			printf("copy is %d \n",u->copies);
			p = 1;
			u = u->next;
		}else if(u-> next == NULL){
			if(strcmp(search, u->authors)==0){
				printf("Id is %d \n",u->id);
				printf("title is %s \n",u->title);
				printf("author is %s \n",u->authors);
				printf("year is %d \n",u->year);
				printf("copy is %d \n",u->copies);
				break;//TODO
			}else{
				if(p == 1){
					break;//TODO
				}
				printf("There are no books entitled %s\n",search);
				return 0;
				break;
			}
		}else{
			u = u->next;
		}
	}
	return 1;
}

int borrow_book(Book*head, char name[30]){
	//printf("enter name");
	//fgets(name,30,stdin);
	char storeid[30];
	char search[30];
	FILE * fp3= fopen("borrow.txt","a+");
	while(!feof(fp3)){
		char storename1[30];
		char storeid1[30];
		fgets(storename1, 30, fp3);
		fgets(storeid1,30,fp3);
		RemoveNewLine(storeid1);
		RemoveNewLine(storename1);
		if(strcmp(storename1,name)==0){
			strcpy(storeid,storeid1);
			break;//TODO
		}
	}
	printf("\nborrowed book id %s\n", storeid);
	printf("\nEnter the ID number of the book you wish to borrow: ");
	//fgets(search,30,stdin);
	scanf(" %s",search);
	RemoveNewLine(search);
	setbuf(stdin,NULL);
	unsigned int y = atoi(search);
	Book*u = head->next;
	while(u){
		if( u->id == y){
			printf("uid is %d\n", u->id);
			if(u->copies != 0){
				u->copies = u->copies - 1;
				strcat(storeid, search);
				printf("id %s",storeid);
				break;
				//TODO
			}
			else{
				printf("This book has been lent out in full");
				break;//TODO
			}
		}else{
			u = u->next;
		}
	}
}

void librarian_view(Book*head){
	int choice;
	printf("Please choose an option:\n");
	printf("1) Add a book\n");
	printf("2) Remove a book\n");
	printf("3) Search for books\n");
	printf("4) Display all books\n");
	printf("5) Log out\n");
	printf("  Option: ");
	scanf("%d", &choice);
	setbuf(stdin,NULL);
	if(choice == 1){
		Book book;
		add(book,head);
	}
	else if(choice == 2){
		int f;
		Book book2;
		printf("Please enter the id of the book: ");
		scanf("%d", &f);
		setbuf(stdin,NULL);
		book2.id = f;
		remove_a_book(book2,head);
	}
	else if(choice ==3){

		printf("\n");
		printf("Loading.....");
		printf("\n");
		int choice2;
		printf("Please select a search method:\n");
		printf("1) Search by title\n");
		printf("2) Search by author\n");
		printf("3) Search by year\n");
		printf(" option:");
		scanf("%d", &choice2);
		setbuf(stdin,NULL);
		if(choice2 == 1){
			printf("\n");
			printf("Loading......");
			printf("\n");
			find_by_title(head);
		}else if(choice2 == 2){
			printf("\n");
			printf("Loading......");
			printf("\n");
			find_by_author(head);
		}else if(choice2 == 3){
			printf("\n");
			printf("Loading......");
			printf("\n");
			find_by_year(head);
		}else{
			printf("\n");
			printf("Loading......");
			printf("\n");
			printf("\n Please enter the correct number.\n");
		}

		printf("\n");
		printf("\n");
		librarian_view(head);
	}
	else if(choice ==4){

		printf("\n");
		printf("Loading......");
		printf("\n");

		Book *t= head->next;
		while(t){
			printf("Id is %d \n",t->id);
			printf("title is %s \n",t->title);
			printf("author is %s \n",t->authors);
			printf("year is %d \n",t->year);
			printf("copy is %d \n",t->copies);
			t=t->next;
		}
	}
	else if(choice == 5){
		//TODO
	}
	else{
		printf("\n Please enter the correct number.\n");
	}
}

int librarian(){
	Book*head;
	CreateNode(head);
	FILE * fp1= fopen("allbook.txt","r");
	allbook(head,fp1);

	librarian_view(head);

}



int main()
{
	librarian();
	/*
	Book*head;
	CreateNode(head);
	FILE * fp1= fopen("allbook.txt","r");
	allbook(head,fp1);

	librarian_view(head);*/
	/*
	char name[30];
	printf("enter name\n");
	fgets(name,30,stdin);
	RemoveNewLine(name);
	setbuf(stdin,NULL);
	borrow_book(head, name);*/
}


